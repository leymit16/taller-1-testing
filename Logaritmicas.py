Python 3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 19:29:22) [MSC v.1916 32 bit (Intel)] on win32
Type "help", "copyright", "credits" or "license()" for more information.
>>> 
>>> import math
 
math.exp(5)                      # returns 148.4131591025766
math.e**5                        # returns 148.4131591025765
 
math.log(148.41315910257657)     # returns 5.0
math.log(148.41315910257657, 2)  # returns 7.213475204444817
math.log(148.41315910257657, 10) # returns 2.171472409516258
 
math.log(1.0000025)              # returns 2.4999968749105643e-06
math.log1p(0.0000025)            # returns 2.4999968750052084e-06
 
math.pow(12.5, 2.8)              # returns 1178.5500657314767
math.pow(144, 0.5)               # returns 12.0
math.sqrt(144)                   # returns 12.0
