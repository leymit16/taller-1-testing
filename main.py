from suma import suma
from resta import resta
from multiplicacion import multiplicacion
from division import division
from porcentaje import Porcentaje
from potencia import potencia
from raiz import raiz

print("Calculadora")
print("Elija su operacion: ")
print("1. suma ")
print("2. resta ")
print("3. multiplicacion ")
print("4. division ")
print("5. porcentaje ")
print("6. potencia ")
print("7. raiz ")
print("8. raiz cuadrada ")
print("9. transformar decimal a fraccion ")
print("0. salir ")
opcion=int(input("introduce tu opcion: "))

while(True):

    if(opcion<0 or opcion>9):
        print("ingrese valor valido")
        opcion=int(input("introduce tu opcion: "))
    elif(opcion==8):
        a=int(input("ingrese el numero: "))
        raiz(a,2)
        opcion=int(input("introduce tu opcion: "))
    elif(opcion==9):
        a=float(input("ingrese valor decimal : "))
        print ("valor fraccion = " ,(a).as_integer_ratio())
        opcion=int(input("introduce tu opcion: "))
    elif(opcion==0):
        break
    else:
        a=int(input("ingrese el primer numero: "))
        b=int(input("ingrese el segundo numero: "))
        if(opcion==1):
            suma(a,b)
            opcion=int(input("introduce tu opcion: "))
        elif(opcion==2):
            resta(a,b)
            opcion=int(input("introduce tu opcion: "))
        elif(opcion==3):
            multiplicacion(a,b)
            opcion=int(input("introduce tu opcion: "))
        elif(opcion==4):
            division(a,b)
            opcion=int(input("introduce tu opcion: "))
        elif(opcion==5):
            Porcentaje(a,b)
            opcion=int(input("introduce tu opcion: "))
        elif(opcion==6):
            potencia(a,b)
            opcion=int(input("introduce tu opcion: "))
        elif(opcion==7):
            raiz(a,b)
            opcion=int(input("introduce tu opcion: "))
